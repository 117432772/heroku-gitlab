const express = require('express')
const app = express()

app.set('PORT', process.env.PORT || 3000)

app.get('/', (req, res) => {
    res.status(200).send({
        message: 'How do I deploy my code to Heroku using GitLab CI/CD? Also, HELLO THERE IS3313. This is ciara my student number is 117432772',
    })
})

app.listen(app.get('PORT'), () =>
    console.log(`Server running on port ${app.get('PORT')}`),
)
